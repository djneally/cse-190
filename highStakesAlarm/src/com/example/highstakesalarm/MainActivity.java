package com.example.highstakesalarm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.highstakesalarm.db.DatabaseHelper;
import com.example.highstakesalarm.model.Alarm;
import com.example.highstakesalarm.model.Tokens;
import com.example.highstakesalarm.receiver.AlarmReceiver;
import com.example.highstakesalarm.service.AlarmService;

public class MainActivity extends ListActivity
{
	SharedPreferences mPrefs;
	final ArrayList seletedItems = new ArrayList();
	AlertDialog dialog;
	final CharSequence[] orgs =
	{ "Republicans", "Democrats", "Libertarians", "Tea Party" };
	final String welcomeScreenShownPref = "welcomeScreenShown";

	private DatabaseHelper db = new DatabaseHelper(this);
	private Context context;
	private AlarmAdapter adapter;
	public Tokens t;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		addListenerOnButtonAdd();
		addListenerOnButtonToken();
		addListenerOnButtonDelete();
		mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		context = this;
		//startService(new Intent(this, AlarmService.class));
		
		Boolean welcomeScreenShown = mPrefs.getBoolean(welcomeScreenShownPref,
				false);

		if (!welcomeScreenShown)
		{
			politicalSurvey();
	//		startServ();
		}
		SharedPreferences.Editor editor = mPrefs.edit();
		editor.putBoolean(welcomeScreenShownPref, true); // Set to true so it
															// doesnt show
															// political party
															// wizard again
		editor.commit();
	//	startServ();

		if(db.getToken(1) == null){
			t = new Tokens(0);
			db.addTokens(t);
		}
		else
			t = db.getToken(1);
		
		db.getAllTokens();
			

	//	String run = isMyServiceRunning(AlarmService.class);
	//	Log.d("check, main onCreate",  "Bing");

		setAdapter();

	}
	
	//BUTTON LISTENERS//
	
	protected void addListenerOnButtonAdd()
	{
		ImageButton button = (ImageButton) findViewById(R.id.addButton);
		button.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Intent i = new Intent(MainActivity.this, editActivity.class);
				Log.d("IDSet", "ID : -1");
				i.putExtra("GET_ID", "-1");
				startActivity(i);
			}
		});
	}
	
	protected void addListenerOnButtonToken()
	{
		ImageButton button = (ImageButton) findViewById(R.id.tokenButton);
		button.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Intent i = new Intent(MainActivity.this, TokenInfoActivity.class);
				startActivity(i);
			}
		});
	}
	protected void addListenerOnButtonDelete()
	{
		Button button = (Button) findViewById(R.id.deleteButton);
		button.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				List<Alarm> alarms = db.getAllAlarms();
				if(!(alarms == null)){
					alarms = sortList(alarms);
				}
				else{
					return;
				}
				for(int counter =  0; counter < alarms.size(); counter++){
					if(alarms.get(counter).isDelete()){
						db.deleteAlarm(alarms.get(counter).getId());
					}
				}
				//Intent i = new Intent(MainActivity.this, MainActivity.class);
				//startActivity(i);
				//adapter.notifyDataSetChanged();
				setAdapter();
			}
		});
	}

///////////////////////////////////	
	//seting things for changes in alarms
	
	private void setAdapter(){
		List<Alarm> alarms = db.getAllAlarms();

		if(!(alarms == null)){
			alarms = sortList(alarms);
		}

		if(super.getListView().getAdapter() == null){
			
		}
		
		adapter = new AlarmAdapter(this, alarms);
		setListAdapter(adapter);
	}
	
	
	public void setAlarmEnabled(long id, boolean isEnabled)
	{
		Alarm model = db.getAlarm(id);
		//Log.d("setEnabled b4", "Enabled: " + model.toString());
		model.isEnabled = isEnabled;
		//Log.d("setEnabled", "Enabled: " + model.toString());
		db.updateAlarm(model);
		//adapter.notifyDataSetChanged();
		setAdapter();
	}
	
	
	public void setAlarmIsDelete(long id, boolean isDelete)
	{
		Alarm model = db.getAlarm(id);
		Log.d("setDelete", "Delete: " + model.toString());

		model.isDelete = isDelete;
		db.updateAlarm(model);
	}

	
	public void startEditAlarmActivity(long id)
	{
		Intent intent = new Intent(MainActivity.this, editActivity.class);
		intent.putExtra("GET_ID", ""+id);
		Log.d("IDSet", "ID : " + id);
		startActivity(intent);
		//startActivityForResult(intent, 0);
	}

	
	/////////////////////////////////
	
	
	
	
	public void politicalSurvey()
	{
		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setTitle("Welcome! Select what organizations you like:");
		adb.setMultiChoiceItems(orgs, null,
				new DialogInterface.OnMultiChoiceClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog,
							int indexSelected, boolean isChecked)
					{
						if (isChecked)
						{
							seletedItems.add(indexSelected);
						}
						else
							if (seletedItems.contains(indexSelected))
							{
								seletedItems.remove(Integer
										.valueOf(indexSelected));
							}
					}
				}).setPositiveButton("OK",
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int id)
					{
						facebookSurvey();
					}
				});

		dialog = adb.create();
		dialog.show();
	}
	
	
	
	
	
	
	public void startFacebookActivity()
	{
		Intent intent = new Intent(MainActivity.this, FacebookActivity.class);
		startActivity(intent);
	}

	
	
	
	
	
	public void facebookSurvey()
	{
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				switch (which)
				{
				case DialogInterface.BUTTON_POSITIVE:
					startFacebookActivity();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					break;
				}
			}
		};

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setMessage(
				"Warning! Would you like to activate facebook wakeup feature? This is an extreme measure"
						+ " that will make sure you will never want to snooze too much. This feature will"
						+ " make it so that after 3 snoozes any subsequent snoozes will have a small chance"
						+ " to post to facebook the organizations you have donated to and the sums!")
				.setPositiveButton("Yes", dialogClickListener)
				.setNegativeButton("No", dialogClickListener).show();
	}
///*	
	
	
	
	
	public List<Alarm> sortList(List<Alarm> alarms){
//		Log.d("sort", "not found");

		if(!alarms.isEmpty()) {
			Collections.sort(alarms, new Comparator<Alarm>() {
				public int compare(final Alarm aOne, final Alarm aTwo) {
					if(aOne.time_hour < aTwo.time_hour){
						return -1;
					}
					if(aOne.time_hour == aTwo.time_hour){
						if(aOne.time_min < aTwo.time_min){
							return -1;
						}
						else if(aOne.time_min > aTwo.time_min){
							return 1;
						}
						else
							return 0;
					}
					else
						return 1;
				}
			});
		}
		
		return alarms;
	}
//*///
	
	private String isMyServiceRunning(Class<?> serviceClass) {
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return "true";
	        }
	    }
	    return "false";
	}
	
	private void startServ(){
		startService(new Intent(this, AlarmService.class));
	}
	
}
