package com.example.highstakesalarm.receiver;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.highstakesalarm.db.DatabaseHelper;
import com.example.highstakesalarm.model.Alarm;
import com.example.highstakesalarm.service.AlarmService;

public class AlarmReceiver extends BroadcastReceiver{

	public static final String ID = "id";
	public static final String HOUR = "hour";
	public static final String MINUTE = "minute";
	public static final String TONE = "alarm";
	public static final String SNOOZE = "snooze";
	public static final String ORG = "donate_to";
	public static final String URI = "alarmURI";
	
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.d("onReceive", "check 1 receiver");

		setAlarms(context);
	}
	
	public static void setAlarms(Context context) {
		// TODO Auto-generated method stub
		cancelAlarms(context);
		
		DatabaseHelper db = new DatabaseHelper(context);
		List<Alarm> alarms = db.getAllAlarms();
		Log.d("setAlarms", "check 1 receiver");
		//Iterator<Alarm> iter = alarms.iterator();
		//Alarm a;
		//used to find next alarm
		final int currentDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		final int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		final int currentMinute = Calendar.getInstance().get(Calendar.MINUTE);
		boolean alarmSet = false;

		//while(iter.hasNext())
		if(!(alarms == null)){
		  for(Alarm a : alarms)
		  {	
			  if((a.time_hour == currentHour) && (a.time_min == currentMinute)){
					Log.d("Receiver", "current time for alarm");
					a.isEnabled = false;
				//	break;
				}
			//a = iter.next();
			if(a.isEnabled)
			{
				Log.d("setAlarms", "check 2 receiver");

				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, a.time_hour);
				cal.set(Calendar.MINUTE, a.time_min);
				cal.set(Calendar.SECOND, 00);
				
				PendingIntent p = createPendingIntent(context, a);

				
				for(int day = Calendar.SUNDAY; day<= Calendar.SATURDAY; day++)
				{
					if(a.getDay(day-1) && day >= currentDay && !(currentDay == day && currentHour > a.time_hour)
														   && !(currentDay == day && currentHour == a.time_hour && currentMinute > a.time_min)) 
					{
						cal.set(Calendar.DAY_OF_WEEK, day);
						Log.d("setAlarms", "check 3 receiver");

						setAlarm(context, cal, p);
						alarmSet = true;
						break;
					}
				}
				
				if(alarmSet == false)
				{
					for(int day = Calendar.SUNDAY; day<= Calendar.SATURDAY; day++)
					{
						if(a.getDay(day-1) && day <= currentDay && a.repeatWeekly) 
						{
							cal.set(Calendar.DAY_OF_WEEK, day);
							cal.add(Calendar.WEEK_OF_YEAR, 1);
							Log.d("setAlarms", "check 4 receiver");

							setAlarm(context, cal, p);
							alarmSet = true;
							break;
						}
					}
				}
			}
			
			if(!(a.isEnabled)) {
				PendingIntent pI = createPendingIntent(context,a);
				Log.d("cancelAlarm", "cancel Alarm");

				AlarmManager alarmMan = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
				alarmMan.cancel(pI);
			}
			
		  }
		}
		
	}
	
	public static void cancelAlarms(Context context) {
		// TODO Auto-generated method stub
		DatabaseHelper db = new DatabaseHelper(context);
		
		List<Alarm> alarms = db.getAllAlarms();
		
		if(alarms != null) { 
			for(Alarm a : alarms) {
				if(a.isEnabled) {
					PendingIntent pI = createPendingIntent(context,a);
					Log.d("cancelAlarms", "cancel Alarm");

					AlarmManager alarmMan = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
					alarmMan.cancel(pI);
				}
			}
		}
	}
	
	//Pending Intent allows the alarm to persist without the application running
	private static PendingIntent createPendingIntent(Context context, Alarm a) {
		// TODO Auto-generated method stub
		Intent i = new Intent(context, AlarmService.class);
		Log.d("pendingIntent", "check 1 receiver");

		
		i.putExtra(ID, a.id);
		i.putExtra(HOUR, a.time_hour);
		i.putExtra(MINUTE, a.time_min);
		i.putExtra(TONE, a.alarm_tone);
		i.putExtra(SNOOZE, a.snooze);
		i.putExtra(ORG,  a.donate.toString());
		i.putExtra(URI, a.uri);
		
		return PendingIntent.getService(context, (int) a.id, i, PendingIntent.FLAG_UPDATE_CURRENT);
	}

	@SuppressLint("NewApi")
	private static void setAlarm(Context context, Calendar calendar, PendingIntent pIntent) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			Log.d("setAlarm", "set Alarm");
			alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pIntent);
		} else {
			Log.d("setAlarm", "set Alarm");

			alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pIntent);
		}
	}
}
