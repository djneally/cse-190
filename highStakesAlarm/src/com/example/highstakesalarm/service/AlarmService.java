package com.example.highstakesalarm.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.example.highstakesalarm.ActivateAlarm;
import com.example.highstakesalarm.MainActivity;
import com.example.highstakesalarm.receiver.AlarmReceiver;

//Used to start activity after alarm is set off
public class AlarmService extends Service {
	
	public static String TAG = AlarmService.class.getSimpleName();
	private boolean running;
	
	@Override
	public void onCreate() {
		super.onCreate();
		running = false;
	}
/*
	@Override
   public void onDestroy() {
		super.onDestroy();
		Toast.makeText(this, "MyAlarmService.onDestroy()", Toast.LENGTH_LONG).show();
   }
//	*/
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("onStartCommand", "check 1 service");
		if(!running)
			running = true;

		Intent alarmInt = new Intent(AlarmService.this, ActivateAlarm.class);
		alarmInt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		alarmInt.putExtras(intent);
		getApplication().startActivity(alarmInt);
		
	//	AlarmReceiver.cancelAlarms(this);
		AlarmReceiver.setAlarms(this);
		
		return super.onStartCommand(intent, flags, startId);
	}

}
