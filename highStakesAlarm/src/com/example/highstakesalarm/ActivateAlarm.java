package com.example.highstakesalarm;


import java.io.IOException;

import com.example.highstakesalarm.db.DatabaseHelper;
import com.example.highstakesalarm.model.Tokens;
import com.example.highstakesalarm.receiver.AlarmReceiver;
import com.example.highstakesalarm.editActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActivateAlarm extends Activity {
	
	public final String TAG = this.getClass().getSimpleName();
	DatabaseHelper db = new DatabaseHelper(this);
	public TextView text;

	private WakeLock wake;
	private MediaPlayer playSound;
	private CountDownTimer cdTimer;
	private Tokens token;

	private static final int WAKELOCK_TIMEOUT = 60 * 1000;
	private final long startTime = 15 * 1000;
	private final long interval = 1 * 1000;
	

	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Setup layout
		this.setContentView(R.layout.activated_screen);
	//	Log.d("onCreate", "check 4");

		db.getAllTokens();
		token = db.getToken(1);
		if(token.tokens != 0){	
		
		playSound = new MediaPlayer();
	//	PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
	//	wake = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Wake Log");

		int hour = getIntent().getIntExtra(AlarmReceiver.HOUR, 0);
		int min = getIntent().getIntExtra(AlarmReceiver.MINUTE, 0);
		String org = getIntent().getStringExtra(AlarmReceiver.ORG);
		
		TextView donate = (TextView) findViewById(R.id.org_name);
		donate.setText("Wake up or you'll contribute to the " + org);
		
		TextView time = (TextView) findViewById(R.id.alarm_time);
		time.setText(String.format("%02d : %02d", hour, min));
		
		text = (TextView) findViewById(R.id.timer);
		cdTimer = new MyCountDownTimer(startTime, interval);
		cdTimer.start();
		
		Button wakeButton = (Button) findViewById(R.id.alarm_screen_button);
		wakeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				playSound.stop();
				finish();
				cdTimer.cancel();
				startActivity(new Intent(ActivateAlarm.this, MainActivity.class));
			}
		});
		
		String tone = getIntent().getStringExtra(AlarmReceiver.URI);
		Uri uri = Uri.parse(tone);
		playSound(this, uri);
		
		Runnable releaseWake = new Runnable() {

			@Override
			public void run() {
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
				getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

				if (wake != null && wake.isHeld()) {
					wake.release();
				}
			}
		};
		
		new Handler().postDelayed(releaseWake, WAKELOCK_TIMEOUT);
		}
		else{
			startActivity(new Intent(ActivateAlarm.this, MainActivity.class));
			Toast.makeText(getApplicationContext(), "Ran out of tokens. Alarms disabled.",
					   Toast.LENGTH_LONG).show();
		}
//*/
	}
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onResume() {
		super.onResume();

		// Set the window to keep screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);

		// Acquire wakelock
		PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
		if (wake == null) {
			wake = pm.newWakeLock((PowerManager.FULL_WAKE_LOCK | PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), TAG);
		}

		if (!wake.isHeld()) {
			wake.acquire();
			Log.i(TAG, "Wakelock aquired!!");
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		if (wake != null && wake.isHeld()) {
			wake.release();
		}
	}
	
	private void playSound(Context context, Uri alert) {
		playSound = new MediaPlayer();
		Log.d("playSound", alert.toString());
		try{
			playSound.setDataSource(context, alert);
			final AudioManager aM = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			if(aM.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
				playSound.setAudioStreamType(AudioManager.STREAM_ALARM);
				playSound.prepare();
				playSound.start();
			}
		} catch (IOException e)
		{
			Log.i("AlarmReceiver", "No audio files are found!");
		}
	}
	
	public class MyCountDownTimer extends CountDownTimer {
		  public MyCountDownTimer(long startTime, long interval) {
		   super(startTime, interval);
		  }
		 
		  @Override
		  public void onFinish() {
			  Toast.makeText(getApplicationContext(), "Wake up or donate again",
					   Toast.LENGTH_LONG).show();
			  cdTimer.cancel();
			  token.tokens = token.tokens - 1;
			  db.updateToken(token);
			  if(token.tokens == 0){
					startActivity(new Intent(ActivateAlarm.this, MainActivity.class));
					Toast.makeText(getApplicationContext(), "Ran out of tokens. Alarms disabled.",
							   Toast.LENGTH_LONG).show();
			  }
			  cdTimer.start();

		  }
		 
		  @Override
		  public void onTick(long millisUntilFinished) {
		   text.setText("You are about to donate in: " + millisUntilFinished / 1000 + " seconds.");
		  }
		 }

}
