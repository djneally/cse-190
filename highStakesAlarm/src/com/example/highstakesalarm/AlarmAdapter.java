package com.example.highstakesalarm;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.highstakesalarm.model.Alarm;

public class AlarmAdapter extends BaseAdapter implements ListAdapter {

	private Context context;
	private List<Alarm> alarms;
	
	public AlarmAdapter(Context c, List<Alarm> a){
	
		this.context = c;
		this.alarms = a;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(alarms != null)
			return alarms.size();
		
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(alarms != null)
			return alarms.get(position);
		
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		if(alarms != null)
			return alarms.get(position).id;
		
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertView == null){
			LayoutInflater in = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = in.inflate(R.layout.alarm_item, parent, false);
		}
		
		Alarm a = (Alarm) getItem(position);
		final long longID = (long) a.id;
		
		TextView time = (TextView) convertView.findViewById(R.id.alarm_time);
//		if(a.time_hour > 12)
//			a.time_hour = a.time_hour - 12;
		time.setText(String.format("%02d : %02d", a.time_hour, a.time_min));
		
		TextView donate = (TextView) convertView.findViewById(R.id.alarm_name);
		donate.setText(a.donate);
		
		TextView sound = (TextView) convertView.findViewById(R.id.alarm_org);
		sound.setText("Alarm Sound: " + a.alarm_tone);
		
		ToggleButton tog = (ToggleButton) convertView.findViewById(R.id.alarm_toggle);
		tog.setChecked(a.isEnabled);
		Log.d("getView", "Enabled: " + a.isEnabled);
		tog.setTag(Long.valueOf(a.id));
		tog.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				((MainActivity) context).setAlarmEnabled(longID, isChecked);
				Log.d("onCheck", "check if toggled");
			}
		});
		
		CheckBox box = (CheckBox) convertView.findViewById(R.id.checkBox1);
		box.setChecked(a.isDelete);
		Log.d("getView","Delete: " + a.isDelete);
		box.setTag(Long.valueOf(a.id));
		box.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isDelete) {
				((MainActivity) context).setAlarmIsDelete(longID, isDelete);
				Log.d("onCheck", "check if Delete");
			}
		});

		
		convertView.setTag(Long.valueOf(a.id));
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View view) {
				Log.d("onEdit", "Edit alarm clicked");
				((MainActivity) context).startEditAlarmActivity(((Long) view.getTag()).longValue());
			}
		});
		
		
		return convertView;
	}
	
	public void setAlarms(List<Alarm> a) {
		this.alarms = a;
	}

	
}
