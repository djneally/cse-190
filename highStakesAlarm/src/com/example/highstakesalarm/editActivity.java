package com.example.highstakesalarm;

import java.util.Iterator;
import java.util.Set;

import com.example.highstakesalarm.db.DatabaseHelper;
import com.example.highstakesalarm.model.Alarm;
import com.example.highstakesalarm.receiver.AlarmReceiver;
import com.example.highstakesalarm.service.AlarmService;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.TimePicker;
import android.widget.NumberPicker.Formatter;
import android.widget.Spinner;

public class editActivity extends Activity
{

	public static SharedPreferences sp;
	public Alarm alarm;
	public DatabaseHelper db;
	public int id;
	public int hour;
	public int min;
	public int snooze;
	public String tone;
	public String donate;
	private TimePicker time;
	public Set<String> day;
	private boolean chkSunday;
	private boolean chkMonday;
	private boolean chkTuesday;
	private boolean chkWednesday;
	private boolean chkThursday;
	private boolean chkFriday;
	private boolean chkSaturday;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.edit_alarm);
		addListenerOnButtonCancel();
		addListenerOnButtonManage();
		addListenerOnButtonSave();
		
		db = new DatabaseHelper(this);
		time = (TimePicker) findViewById(R.id.timePicker2);
		alarm = new Alarm();

		String newString= getIntent().getStringExtra("GET_ID");
		id = Integer.parseInt(newString);
		Log.d("Received properly", "Received ID : " + id);
		

		if(id == -1)
			alarm = new Alarm();
		else{
			alarm = db.getAlarm(id);
			
			time.setCurrentMinute(alarm.time_min);
			time.setCurrentHour(alarm.time_hour);
		}
			/*
			chkWeekly.setChecked(alarmDetails.repeatWeekly);
			chkSunday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.SUNDAY));
			chkMonday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.MONDAY));
			chkTuesday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.TUESDAY));
			chkWednesday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.WEDNESDAY));
			chkThursday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.THURSDAY));
			chkFriday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.FRDIAY));
			chkSaturday.setChecked(alarmDetails.getRepeatingDay(AlarmModel.SATURDAY));
*/
			//tone = RingtoneManager.getRingtone(this, Uri.parse(alarm.alarm_tone)).getTitle(this);
	//	}
	}

	protected void addListenerOnButtonCancel()
	{
		ImageButton button = (ImageButton) findViewById(R.id.cancelButton);
		button.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Intent i = new Intent(editActivity.this, MainActivity.class);
				startActivity(i);
			}
		});
	}

	protected void addListenerOnButtonManage()
	{
		ImageButton button = (ImageButton) findViewById(R.id.managePref);
		button.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Intent i = new Intent(editActivity.this,
						preferenceActivity.class);
				startActivity(i);
			}
		});
	}

	protected void addListenerOnButtonSave()
	{
		ImageButton button = (ImageButton) findViewById(R.id.SaveButton);
		button.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				updateFromLayout();
				AlarmReceiver.cancelAlarms(editActivity.this);
				if(alarm.id < 0)
					db.addAlarm(alarm);
				else
					db.updateAlarm(alarm);
				AlarmReceiver.setAlarms(editActivity.this);

				Intent i = new Intent(editActivity.this, MainActivity.class);
				startActivity(i);
			}
		});
	}

	private void updateFromLayout()
	{

		sp = PreferenceManager.getDefaultSharedPreferences(this);
		donate = sp.getString("donate_to", "No Group");
		tone = sp.getString("ringtone_pref", "No Sound");
		String soundName = RingtoneManager.getRingtone(this, Uri.parse(tone)).getTitle(this);
		boolean weekly = sp.getBoolean("repeat_pref", true);
				
		//String day = checkDays();
		//Log.d("updateFromLayout", day);

		alarm.time_min = time.getCurrentMinute().intValue();
		alarm.time_hour = time.getCurrentHour().intValue();
		alarm.repeatWeekly = weekly;
		//Log.d("updateFromLayout", "check 1");

		alarm.setDay(Alarm.SUNDAY, true);
		//Log.d("updateFromLayout", "check 2");

		alarm.setDay(Alarm.MONDAY, true);
		alarm.setDay(Alarm.TUESDAY, true);
		alarm.setDay(Alarm.WEDNESDAY, true);
		alarm.setDay(Alarm.THURSDAY, true);
		alarm.setDay(Alarm.FRDIAY, true);
		alarm.setDay(Alarm.SATURDAY, true);
		//*/
		alarm.donate = donate;
		alarm.alarm_tone = soundName;
		alarm.uri = tone;
		alarm.isEnabled = true;
	}
	
	private String checkDays(){
		day = sp.getStringSet("repeatDays", null);
		
		Iterator<String> iter = day.iterator();
		String day;
		int i = 0;
		
		while(iter.hasNext()){
			day = iter.next();
			
			if(day == "Sunday"){
				chkSunday = true;
				i++;
			}
			
			if(day == "Monday"){
				chkMonday = true;
				i++;
			}
			
			if(day == "Tuesday"){
				chkTuesday = true;
				i++;
			}
			
			if(day == "Wednesday"){
				chkWednesday = true;
				i++;
			}
			
			if(day == "Thursday"){
				chkThursday = true;
				i++;
			}
			
			if(day == "Friday"){
				chkFriday = true;
				i++;
			}
			
			if(day == "Saturday"){
				chkSaturday = true;
				i++;
			}
		}
		
		if(i != 0)
			return "true";
		
		else
			return "false";

	}

}
