package com.example.highstakesalarm.db;


import java.util.LinkedList;
import java.util.List;

import com.example.highstakesalarm.model.Alarm;
import com.example.highstakesalarm.model.Tokens;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper{

	private static final String DATABASE_NAME = "alarms";
	private static final int DATABASE_VERSION = 1;
	
	private static final String TABLE_ALARM = "alarmTable";
	private static final String TABLE_TOKENS = "tokenTable";
		
	private static final String KEY_TOKENS = "tokens";
	private static final String KEY_HOUR = "hours";
	private static final String KEY_MIN = "minutes";
	private static final String KEY_ALARM_SOUND = "alarm";
	private static final String KEY_SNOOZE = "snoozes";
	private static final String KEY_ID_ALARM = "idA";
	private static final String KEY_ENABLED = "isEnabled";
	private static final String KEY_ID_TOKEN = "idT";
	private static final String KEY_ORG = "org";
	private static final String KEY_DAYS = "day";
	private static final String KEY_REPEAT_WEEK = "repeat_weekly";
	private static final String KEY_DELETE = "isDelete";
	private static final String KEY_URI = "alarmURI";
	
    private static final String[] ALARM_COLUMNS = {KEY_ID_ALARM,    //1
    											   KEY_HOUR,        //2
    											   KEY_MIN,         //3
    											   KEY_ALARM_SOUND, //4
    											   KEY_SNOOZE,      //5
    											   KEY_ORG,         //6
    											   KEY_DAYS,        //7
    											   KEY_REPEAT_WEEK, //8
    											   KEY_ENABLED,		//9
    											   KEY_DELETE,      //10
    											   KEY_URI};        //11
    
    private static final String[] TOKEN_COLUMNS = {KEY_ID_TOKEN, 
    											   KEY_TOKENS};

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String CREATE_ALARM_TABLE = "CREATE TABLE alarmTable( " +
				"idA INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"hours INTEGER, " +
				"minutes INTEGER, " +
				"alarm STRING, " +
				"snoozes INTEGER, " +
				"org STRING, " + 
				"day STRING, " +
				"repeat_weekly STRING, " +
				"isEnabled STRING," +
				"isDelete STRING," +
				"alarmURI STRING)";
		
		db.execSQL(CREATE_ALARM_TABLE);
		
	//	Log.d("onCreate db", "made alarms db");
		
		String CREATE_TOKEN_TABLE = "CREATE TABLE tokenTable( " +
				"idT INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"tokens INTEGER )";
		
		
		db.execSQL(CREATE_TOKEN_TABLE);
	//	Log.d("onCreate db", "made alarms db");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS alarmTable");
		db.execSQL("DROP TABLE IF EXISTS tokenTable");
		
		onCreate(db);
	}
	
	//helper method to reduce the reuse of code
	private Alarm makeAlarm(Cursor c){
		Alarm alarm = new Alarm();
		
		alarm.setId(c.getInt(c.getColumnIndex(KEY_ID_ALARM)));
		alarm.setHour(c.getInt(c.getColumnIndex(KEY_HOUR)));
		alarm.setMin(c.getInt(c.getColumnIndex(KEY_MIN)));
		alarm.setSnooze(c.getInt(c.getColumnIndex(KEY_SNOOZE)));
		alarm.alarm_tone = c.getString(c.getColumnIndex(KEY_ALARM_SOUND));
		alarm.donate = c.getString(c.getColumnIndex(KEY_ORG));
		alarm.repeatWeekly = c.getInt(c.getColumnIndex(KEY_REPEAT_WEEK)) == 0 ? false : true;
		alarm.isEnabled = c.getInt(c.getColumnIndex(KEY_ENABLED)) == 0 ? false : true;
		alarm.isDelete = c.getInt(c.getColumnIndex(KEY_DELETE)) == 0 ? false : true;
		alarm.uri = c.getString(c.getColumnIndex(KEY_URI));
		
		String[] days = c.getString(c.getColumnIndex(KEY_DAYS)).split(",");
		for (int i = 0; i < days.length; ++i) {
			alarm.setDay(i, days[i].equals("false") ? false : true);
		}
		
		return alarm;
	}
	
	//helper method used to reduce the reuse of code
	private ContentValues makeContent(Alarm alarm){
		ContentValues values = new ContentValues();
		
		values.put(KEY_HOUR, alarm.getHour());
		values.put(KEY_MIN, alarm.getMin());
		values.put(KEY_ALARM_SOUND, alarm.getTone());
		values.put(KEY_SNOOZE, alarm.getSnooze());
		values.put(KEY_ORG, alarm.donate);
		values.put(KEY_ENABLED, alarm.isEnabled);
		values.put(KEY_REPEAT_WEEK, alarm.repeatWeekly);
		values.put(KEY_DELETE, alarm.isDelete);
		values.put(KEY_URI, alarm.uri);
		
		String days = "";
        for (int i = 0; i < 7; ++i) {
        	days += alarm.getDay(i) + ",";
        }
        values.put(KEY_DAYS, days);
		
		return values;
	}
	
	private ContentValues makeConToken(Tokens token){
		ContentValues values = new ContentValues();
		
		values.put(KEY_TOKENS, token.getTokens());
		
		return values;
	}
	
	private Tokens makeToken(Cursor c){
		Tokens token = new Tokens();
		
		token.id = c.getInt(c.getColumnIndex(KEY_ID_TOKEN));
		token.tokens = c.getInt(c.getColumnIndex(KEY_TOKENS));
		
		return token;
	}
	
		
	//helper method to check if alarm is already in the table
	private int checkAlarmId(Alarm a){
		List<Alarm> alarms = getAllAlarms();
		
		Alarm b = new Alarm();
		for(int i = 0; i<alarms.size(); i++){
			b = alarms.get(i);
			if(a.getHour() == b.getHour()){
				if(a.getMin() == b.getMin()){
					return b.id;
				}
			}
				
		}
		return -1;
	} //*/
	
	public void addAlarm(Alarm alarm){
		SQLiteDatabase db = this.getWritableDatabase();
		
		Log.d("addAlarm", alarm.toString());
		
		ContentValues values = makeContent(alarm);
				
		db.insert(TABLE_ALARM, null, values);
		
		//Log.d("addAlarm", alarm.toString());
		
		db.close();
	}
	
	public void addTokens(Tokens token){
		SQLiteDatabase db = this.getWritableDatabase();
		
		Log.d("addTokens", token.toString());

		ContentValues values = makeConToken(token);
		
		db.insert(TABLE_TOKENS, null, values);
		
		db.close();
	}
	
	public List<Alarm> getAllAlarms() {
	       List<Alarm> alarms = new LinkedList<Alarm>();
	 
	       // 1. build the query
	       String query = "SELECT  * FROM " + TABLE_ALARM;
	 
	       // 2. get reference to writable DB
	       SQLiteDatabase db = this.getWritableDatabase();
	       Cursor c = db.rawQuery(query, null);
	 
	       // 3. go over each row, build alarm and add it to list
	       while(c.moveToNext()) {
	    	   alarms.add(makeAlarm(c));
	       }
	 
	      // Log.d("getAllAlarms()", alarms.toString());
	 
	       // return alarms
	       if(!alarms.isEmpty())
	    	   return alarms;
	       
	       else
	    	   return null;
	   }
	
	public List<Tokens> getAllTokens() {
	       List<Tokens> tokens = new LinkedList<Tokens>();
	 
	       // 1. build the query
	       String query = "SELECT  * FROM " + TABLE_TOKENS;
	 
	       // 2. get reference to writable DB
	       SQLiteDatabase db = this.getWritableDatabase();
	       Cursor c = db.rawQuery(query, null);
	 
	       // 3. go over each row, build alarm and add it to list
	       while(c.moveToNext()) {
	    	   tokens.add(makeToken(c));
	       }
	 
	       Log.d("getAllTokens()", tokens.toString());
	 
	       // return alarms
	       if(!tokens.isEmpty())
	    	   return tokens;
	       
	       else
	    	   return null;
	   }
	
	public Alarm getAlarm(Alarm a){
		SQLiteDatabase db = this.getReadableDatabase();
		String SELECT_ALARM = "SELECT * FROM %s " + "WHERE %s = %d";
		//Log.d("getAlarm", "check 2");
		
		int id = checkAlarmId(a);
		
		String query = String.format(SELECT_ALARM, TABLE_ALARM, KEY_ID_ALARM, id);
		//Log.d("getAlarm", "check one");
		Cursor c = db.rawQuery(query, null);
		//Log.d("getAlarm", "check 3");

		if(c.moveToFirst()){
			//Log.d("getAlarm", "found alarm" + makeAlarm(c).toString());

			return makeAlarm(c); 
		}
			
		else {
			Log.d("getAlarm", "not found");

			return null;
		}	
	}
	
	public Alarm getAlarm(long id){
		SQLiteDatabase db = this.getReadableDatabase();
		String SELECT_ALARM = "SELECT * FROM %s " + "WHERE %s = %d";
		//Log.d("getAlarm", "check 2");
		
		//int id = checkAlarmId(a);
		
		String query = String.format(SELECT_ALARM, TABLE_ALARM, KEY_ID_ALARM, id);
		//Log.d("getAlarm", "check one");
		Cursor c = db.rawQuery(query, null);
		//Log.d("getAlarm", "check 3");

		if(c.moveToFirst()){
			//Log.d("getAlarm", "found alarm" + makeAlarm(c).toString());

			return makeAlarm(c); 
		}
			
		else {
			Log.d("getAlarm", "not found");

			return null;
		}	
	}
	
	public Tokens getToken(int id){
		SQLiteDatabase db = this.getReadableDatabase();
		String SELECT_TOKEN = "SELECT * FROM %s " + "WHERE %s = %d";
		//Log.d("getAlarm", "check 2");
		
		//int id = checkAlarmId(a);
		
		String query = String.format(SELECT_TOKEN, TABLE_TOKENS, KEY_ID_TOKEN, id);
	//	Log.d("getToken", "check one");
		Cursor c = db.rawQuery(query, null);
	//	Log.d("getToken", "check 3");

		if(c.moveToFirst()){
	//		Log.d("getToken", "found toke" + makeToken(c).toString());

			return makeToken(c); 
		}
			
		else {
			Log.d("getToken", "not found");

			return null;
		}	
	}
	
	public int updateAlarm(Alarm a){
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues value = makeContent(a);
		
		int i = db.update(TABLE_ALARM, 
				value, 
				"idA = ?", 
				new String[] { String.valueOf(a.getId()) });
		Log.d("update", "check one");

		db.close();
		
		return i;
	}
	
	public void updateAlarm(int id, Alarm a){
		SQLiteDatabase db = this.getWritableDatabase();
		
		//ContentValues value = makeContent(a);
		this.deleteAlarm(id);
		this.addAlarm(a);
		
		Log.d("update", "check one");

		db.close();
		
	//	return i;
	}
	
	public int updateToken(Tokens t){
		SQLiteDatabase db = this.getWritableDatabase();
	
		ContentValues values = new ContentValues();
		values.put(KEY_TOKENS, t.getTokens());
		
		int i = db.update(TABLE_TOKENS, 
				values, 
				KEY_ID_TOKEN+ " = ?", 
				new String[] { String.valueOf(t.getId()) });
		
		db.close();
		Log.d("update", "check one");

		return i;
	}
	
	public void deleteAlarm(Alarm a) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(TABLE_ALARM, 
				KEY_ID_ALARM + " = ?", 
				new String[] { String.valueOf(a.getId()) });
		
		Log.d("deleteAlarm", "Removed alarm "+a+" from database.");
		db.close();
	}
	
	public int deleteAlarm(int id) {
		SQLiteDatabase db = this.getWritableDatabase();

		return db.delete(TABLE_ALARM, 
				KEY_ID_ALARM + " = ?", 
				new String[] { String.valueOf(id) });
		
		//Log.d("deleteAlarm", "Removed alarm "+a+" from database.");		
	}
	
	public void deleteAllAlarms(){
		SQLiteDatabase db = this.getWritableDatabase();
		
		String query = "SELECT  * FROM " + TABLE_ALARM;
		 
	    // 2. get reference to writable DB
	    
	    Cursor c = db.rawQuery(query, null);
	 
	    // 3. go over each row, build alarm and add it to list
	    while(c.moveToNext()) {
	       deleteAlarm(makeAlarm(c));
	     }
	    
	    //KEY_ID_ALARM = "0";
	}
	
	public void deleteToken(Tokens t) {
		SQLiteDatabase db = this.getWritableDatabase();

		db.delete(TABLE_TOKENS, 
				KEY_ID_TOKEN + " = ?", 
				new String[] { String.valueOf(t.getId()) });
		
		db.close();
	}
	
	public void deleteAllTokens(){
		SQLiteDatabase db = this.getWritableDatabase();
		
		String query = "SELECT  * FROM " + TABLE_TOKENS;
		 
	    // 2. get reference to writable DB
	    
	    Cursor c = db.rawQuery(query, null);
	 
	    // 3. go over each row, build alarm and add it to list
	    while(c.moveToNext()) {
	       deleteToken(makeToken(c));
	     }
	    
	    Log.d("deleteAllTokens", "delete all");
	    
	    //KEY_ID_ALARM = "0";
	}
}
