package com.example.highstakesalarm;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.highstakesalarm.db.DatabaseHelper;
import com.example.highstakesalarm.model.Tokens;

public class TokenInfoActivity extends Activity
{

	private FrameLayout layout_MainMenu;
	DatabaseHelper db = new DatabaseHelper(this);
	Tokens token;

	int db_numtokens;

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.token_info);

		if (db.getToken(1) == null)
		{
			token = new Tokens(0);
			db.addTokens(token);
		}
		else
		{
			token = db.getToken(1);
		}

		db_numtokens = token.tokens;

//		Toast.makeText(getApplicationContext(),
//				"There are " + db_numtokens + " tokens", Toast.LENGTH_SHORT)
//				.show();

		final TextView tokentext = (TextView) findViewById(R.id.tokentext);
		tokentext.setText("You Have " + db_numtokens + " Tokens");

		layout_MainMenu = (FrameLayout) findViewById(R.id.mainmenu);
		layout_MainMenu.getForeground().setAlpha(0);

		final Button button10 = (Button) findViewById(R.id.buttonid_10);
		button10.setOnClickListener(new Button.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				layout_MainMenu.getForeground().setAlpha(220);
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(LAYOUT_INFLATER_SERVICE);

				View popup10 = layoutInflater.inflate(R.layout.tentokens, null);

				Display display = getWindowManager().getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				int width = size.x;
				int height = size.y;

				final PopupWindow popupWindow = new PopupWindow(popup10);
				popupWindow.setWidth(width - 100);
				popupWindow.setHeight(450);

				Button btnDismiss = (Button) popup10.findViewById(R.id.dismiss);

				btnDismiss.setOnClickListener(new Button.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{

						popupWindow.dismiss();

						LayoutInflater layoutInflaterSuccess = (LayoutInflater) getBaseContext()
								.getSystemService(LAYOUT_INFLATER_SERVICE);

						View success = layoutInflaterSuccess.inflate(
								R.layout.success, null);

						Display displaySuccess = getWindowManager()
								.getDefaultDisplay();
						Point sizeSuccess = new Point();
						displaySuccess.getSize(sizeSuccess);
						int widthSuccess = sizeSuccess.x;
						int heightSuccess = sizeSuccess.y;

						final PopupWindow popupWindowSuccess = new PopupWindow(
								success);
						popupWindowSuccess.setWidth(widthSuccess - 100);
						popupWindowSuccess.setHeight(450);

						Button btnDismissSuccess = (Button) success
								.findViewById(R.id.dismiss666);

						btnDismissSuccess
								.setOnClickListener(new Button.OnClickListener()
								{

									@Override
									public void onClick(View v)
									{
										token.tokens = token.tokens + 10;
										db.updateToken(token);
										db_numtokens = token.tokens;
										tokentext.setText("You Have "
												+ db_numtokens + " Tokens");

										layout_MainMenu.getForeground()
												.setAlpha(0); // restore
										// TODO Auto-generated method stub
										popupWindowSuccess.dismiss();
									}
								});

						popupWindowSuccess.showAtLocation(success,
								Gravity.CENTER, 0, 0);

					}
				});

				popupWindow.showAtLocation(popup10, Gravity.CENTER, 0, 0);

			}
		});

		final Button button25 = (Button) findViewById(R.id.buttonid_25);
		button25.setOnClickListener(new Button.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				layout_MainMenu.getForeground().setAlpha(220);
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(LAYOUT_INFLATER_SERVICE);

				View popup25 = layoutInflater.inflate(
						R.layout.twentyfivetokens, null);

				Display display = getWindowManager().getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				int width = size.x;
				int height = size.y;

				final PopupWindow popupWindow = new PopupWindow(popup25);
				popupWindow.setWidth(width - 100);
				popupWindow.setHeight(450);

				Button btnDismiss = (Button) popup25
						.findViewById(R.id.dismiss5);

				btnDismiss.setOnClickListener(new Button.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{

						popupWindow.dismiss();

						LayoutInflater layoutInflaterSuccess = (LayoutInflater) getBaseContext()
								.getSystemService(LAYOUT_INFLATER_SERVICE);

						View success = layoutInflaterSuccess.inflate(
								R.layout.success, null);

						Display displaySuccess = getWindowManager()
								.getDefaultDisplay();
						Point sizeSuccess = new Point();
						displaySuccess.getSize(sizeSuccess);
						int widthSuccess = sizeSuccess.x;
						int heightSuccess = sizeSuccess.y;

						final PopupWindow popupWindowSuccess = new PopupWindow(
								success);
						popupWindowSuccess.setWidth(widthSuccess - 100);
						popupWindowSuccess.setHeight(450);

						Button btnDismissSuccess = (Button) success
								.findViewById(R.id.dismiss666);

						btnDismissSuccess
								.setOnClickListener(new Button.OnClickListener()
								{

									@Override
									public void onClick(View v)
									{

										token.tokens = token.tokens + 25;
										db.updateToken(token);
										db_numtokens = token.tokens;

										tokentext.setText("You Have "
												+ db_numtokens + " Tokens");

										layout_MainMenu.getForeground()
												.setAlpha(0); // restore
										// TODO Auto-generated method stub
										popupWindowSuccess.dismiss();
									}
								});

						popupWindowSuccess.showAtLocation(success,
								Gravity.CENTER, 0, 0);

					}
				});

				popupWindow.showAtLocation(popup25, Gravity.CENTER, 0, 0);

			}
		});

		final Button button45 = (Button) findViewById(R.id.buttonid_45);
		button45.setOnClickListener(new Button.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				layout_MainMenu.getForeground().setAlpha(220);
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(LAYOUT_INFLATER_SERVICE);

				View popup45 = layoutInflater.inflate(R.layout.fortyfivetokens,
						null);

				Display display = getWindowManager().getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				int width = size.x;
				int height = size.y;

				final PopupWindow popupWindow = new PopupWindow(popup45);
				popupWindow.setWidth(width - 100);
				popupWindow.setHeight(450);

				Button btnDismiss = (Button) popup45
						.findViewById(R.id.dismiss2);

				btnDismiss.setOnClickListener(new Button.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{

						popupWindow.dismiss();

						LayoutInflater layoutInflaterSuccess = (LayoutInflater) getBaseContext()
								.getSystemService(LAYOUT_INFLATER_SERVICE);

						View success = layoutInflaterSuccess.inflate(
								R.layout.success, null);

						Display displaySuccess = getWindowManager()
								.getDefaultDisplay();
						Point sizeSuccess = new Point();
						displaySuccess.getSize(sizeSuccess);
						int widthSuccess = sizeSuccess.x;
						int heightSuccess = sizeSuccess.y;

						final PopupWindow popupWindowSuccess = new PopupWindow(
								success);
						popupWindowSuccess.setWidth(widthSuccess - 100);
						popupWindowSuccess.setHeight(450);

						Button btnDismissSuccess = (Button) success
								.findViewById(R.id.dismiss666);

						btnDismissSuccess
								.setOnClickListener(new Button.OnClickListener()
								{

									@Override
									public void onClick(View v)
									{
										token.tokens = token.tokens + 45;
										db.updateToken(token);
										db_numtokens = token.tokens;

										tokentext.setText("You Have "
												+ db_numtokens + " Tokens");

										layout_MainMenu.getForeground()
												.setAlpha(0); // restore
										// TODO Auto-generated method stub
										popupWindowSuccess.dismiss();
									}
								});

						popupWindowSuccess.showAtLocation(success,
								Gravity.CENTER, 0, 0);

					}
				});

				popupWindow.showAtLocation(popup45, Gravity.CENTER, 0, 0);

			}
		});

		final Button button70 = (Button) findViewById(R.id.buttonid_70);
		button70.setOnClickListener(new Button.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				layout_MainMenu.getForeground().setAlpha(220);
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(LAYOUT_INFLATER_SERVICE);

				View popup70 = layoutInflater.inflate(R.layout.seventytokens,
						null);

				Display display = getWindowManager().getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				int width = size.x;
				int height = size.y;

				final PopupWindow popupWindow = new PopupWindow(popup70);
				popupWindow.setWidth(width - 100);
				popupWindow.setHeight(450);

				Button btnDismiss = (Button) popup70
						.findViewById(R.id.dismiss4);

				btnDismiss.setOnClickListener(new Button.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{

						popupWindow.dismiss();

						LayoutInflater layoutInflaterSuccess = (LayoutInflater) getBaseContext()
								.getSystemService(LAYOUT_INFLATER_SERVICE);

						View success = layoutInflaterSuccess.inflate(
								R.layout.success, null);

						Display displaySuccess = getWindowManager()
								.getDefaultDisplay();
						Point sizeSuccess = new Point();
						displaySuccess.getSize(sizeSuccess);
						int widthSuccess = sizeSuccess.x;
						int heightSuccess = sizeSuccess.y;

						final PopupWindow popupWindowSuccess = new PopupWindow(
								success);
						popupWindowSuccess.setWidth(widthSuccess - 100);
						popupWindowSuccess.setHeight(450);

						Button btnDismissSuccess = (Button) success
								.findViewById(R.id.dismiss666);

						btnDismissSuccess
								.setOnClickListener(new Button.OnClickListener()
								{

									@Override
									public void onClick(View v)
									{
										token.tokens = token.tokens + 70;
										db.updateToken(token);
										db_numtokens = token.tokens;

										tokentext.setText("You Have "
												+ db_numtokens + " Tokens");

										layout_MainMenu.getForeground()
												.setAlpha(0); // restore
										// TODO Auto-generated method stub
										popupWindowSuccess.dismiss();
									}
								});

						popupWindowSuccess.showAtLocation(success,
								Gravity.CENTER, 0, 0);

					}
				});

				popupWindow.showAtLocation(popup70, Gravity.CENTER, 0, 0);

			}
		});

		final Button button100 = (Button) findViewById(R.id.buttonid_100);
		button100.setOnClickListener(new Button.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{

				layout_MainMenu.getForeground().setAlpha(220);
				LayoutInflater layoutInflater = (LayoutInflater) getBaseContext()
						.getSystemService(LAYOUT_INFLATER_SERVICE);

				View popup100 = layoutInflater.inflate(
						R.layout.onehundredtokens, null);

				Display display = getWindowManager().getDefaultDisplay();
				Point size = new Point();
				display.getSize(size);
				int width = size.x;
				int height = size.y;

				final PopupWindow popupWindow = new PopupWindow(popup100);
				popupWindow.setWidth(width - 100);
				popupWindow.setHeight(450);

				Button btnDismiss = (Button) popup100
						.findViewById(R.id.dismiss3);

				btnDismiss.setOnClickListener(new Button.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{

						popupWindow.dismiss();

						LayoutInflater layoutInflaterSuccess = (LayoutInflater) getBaseContext()
								.getSystemService(LAYOUT_INFLATER_SERVICE);

						View success = layoutInflaterSuccess.inflate(
								R.layout.success, null);

						Display displaySuccess = getWindowManager()
								.getDefaultDisplay();
						Point sizeSuccess = new Point();
						displaySuccess.getSize(sizeSuccess);
						int widthSuccess = sizeSuccess.x;
						int heightSuccess = sizeSuccess.y;

						final PopupWindow popupWindowSuccess = new PopupWindow(
								success);
						popupWindowSuccess.setWidth(widthSuccess - 100);
						popupWindowSuccess.setHeight(450);

						Button btnDismissSuccess = (Button) success
								.findViewById(R.id.dismiss666);

						btnDismissSuccess
								.setOnClickListener(new Button.OnClickListener()
								{

									@Override
									public void onClick(View v)
									{

										token.tokens = token.tokens + 100;
										db.updateToken(token);
										db_numtokens = token.tokens;

										tokentext.setText("You Have "
												+ db_numtokens + " Tokens");

										layout_MainMenu.getForeground()
												.setAlpha(0); // restore
										// TODO Auto-generated method stub
										popupWindowSuccess.dismiss();
									}
								});

						popupWindowSuccess.showAtLocation(success,
								Gravity.CENTER, 0, 0);

					}
				});

				popupWindow.showAtLocation(popup100, Gravity.CENTER, 0, 0);

			}
		});

	}
}
