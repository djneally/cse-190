package com.example.highstakesalarm.model;

import android.net.Uri;

public class Alarm {
	
	public static final int SUNDAY = 0;
	public static final int MONDAY = 1;
	public static final int TUESDAY = 2;
	public static final int WEDNESDAY = 3;
	public static final int THURSDAY = 4;
	public static final int FRDIAY = 5;
	public static final int SATURDAY = 6;
	
	private boolean days[];
	public boolean repeatWeekly;
	public int id = -1;
	public int time_hour;
	public int time_min;
	public int snooze;
	public String alarm_tone;
	public boolean isEnabled;
	public String donate;
	public boolean isDelete;
	public String uri;
	
	public Alarm(){
		this.days = new boolean[7];
		
	}
	
	public Alarm(int tHour, int tMin, String tone) {
		this.time_hour = tHour;
		this.time_min = tMin;
		this.alarm_tone = tone;
		this.snooze = 0;
		this.days = new boolean[7];

	}
	
	//ID get and set
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	//Hour get and set
	public int getHour() {
		return this.time_hour;
	}
	
	public void setHour(int newTime) {
		this.time_hour = newTime;
	}
	
	//Minute get and set
	public int getMin() {
		return this.time_min;
	}
	
	public void setMin(int newTime) {
		this.time_min = newTime;
	}

	//Snooze get and set
	public int getSnooze() {
		return this.snooze;
	}
	
	public void setSnooze(int count) {
		this.snooze = count;
	}
	
	public void incSnooze() {
		this.snooze = this.snooze + 1;
	}
	
	public void resetSnooze(){
		this.snooze = 0;
	}
	
	//alarm sound get and set
	public String getTone() {
		return this.alarm_tone;
	}
	
	public void setTone(String newTone) {
		this.alarm_tone = newTone;
	}
	
	//repeating days set and get
	public void setDay(int dayOfWeek, boolean value) {
		days[dayOfWeek] = value;
	}
	
	public boolean getDay(int dayOfWeek) {
		return days[dayOfWeek];
	}
	
	public String toString(){
		return "Alarm [id:"+this.id +
				", hour:"+this.time_hour+
				", minute:"+this.time_min+
				", snoozes:"+this.snooze+
				", tone:"+this.alarm_tone+
				", enabled:"+this.isEnabled+"] \n";
	}

	public boolean isDelete() {
		return this.isDelete;
	}
}
