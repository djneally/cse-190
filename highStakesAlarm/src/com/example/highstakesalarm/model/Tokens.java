package com.example.highstakesalarm.model;

public class Tokens {

	public int tokens;
	public int id;
	
	public Tokens(){}
	
	public Tokens(int count) {
		this.tokens = count;
	}
	
	//ID get and set
	public int getId(){
		return this.id;
	}
		
	public void setId(int id){
		this.id = id;
	}
	
	public int getTokens() {
		return this.tokens;
	}
	
	public void setTokens(int count) {
		this.tokens = count;
	}
	
	public void resetTokens() {
		this.tokens = 0;
	}
	
	public void addTokens(int newTokens) {
		this.tokens = this.tokens + newTokens;
	}
	
	public String toString(){
		return "Tokens[id:"+this.id+", tokens:"+this.tokens+" ]";
	}
}
